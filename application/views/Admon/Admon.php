<div class="row-fluid">
	<div class="row col-md-10">
		<h1>Administrador</h1>
	</div>
	<div class="row col-md-10" id="Opciones" >
		<div class="btn btn-default btn-lg" id="Alta" role="button">Altas</div>
		<div class="btn btn-default btn-lg" id="Baja"  role="button">Bajas</div>
		<div class="btn btn-default btn-lg" id="Modificacion" role="button">Modificaciones</div>
	</div>

	<div class="col-md-10" id="Altas" style="display:none">
		<div class="row col-md-9" id="altasOpciones">
			<a class="btn btn-default btn-lg" href="#" id="Libro" role="button">Libro</a>
			<a class="btn btn-default btn-lg" href="#" id="Autor" role="button">Autor</a>
			<a class="btn btn-default btn-lg" href="#" id="Editorial" role="button">Editorial</a>
		</div>
		<div id="LibroF" style="display:none" class="row col-md-9">
			<form action="<?php echo base_url('admon/altaLibro'); ?> " method="post" class="row col-md 7">
					<p>Nombre Libro: </p>
					<div class="form-group">
						<input type="text" name="libro"  required class="form-control" placeholder="Ingrese el nombre del libro">
					</div>
					<br>	
					<p>Autor:</p> 	
					<div class="form-group">
						<select name="autor" id="SelectAutor"  required  class="form-control">
							<option value="">Escoje el autor</option>
							<?php 
								foreach ($autores as $key => $autor) {
									# code...
									echo '<option value="'.$autor["idAutor"].'">'.$autor["nombreAutor"].'</option>';
								}
							 ?>
						</select>
					</div>
					<br>
					<p>Editorial:</p> 
					<div class="form-group">
						<select name="editorial" id="SelectAutor"  required class="form-control">
							<option value="0">Escoje la editorial</option>
							<?php 
								foreach ($editoriales as $key => $editorial) {
									# code...
									echo '<option value="'.$editorial["id"].'">'.$editorial["nombreEditorial"].'</option>';
								}
							 ?>
						</select>
					</div>
					<input type="submit" id="btnAlta" class="btn btn-primary" name="" value="Registar">
			
			</form>	


		</div>
		<div id="autorF" style="display:none" class="row  col-md-9">
			<form action="<?php echo base_url('admon/altaAutor'); ?>" method="post" class="form-group">
				<p>Nombre autor:</p>
				<input type="text" name="nombreAutor"  required class="form-control" placeholder="Ingrese nombre"><br>
				<p>Apellido Paterno:</p>
				<input type="text" name="apAutor" required  class="form-control" placeholder="Ingrese Apellido Paterno"><br>
				<p>Apellido Materno:</p>
				<input type="text" name="amAutor" required class="form-control" placeholder="Ingrse Apellido Materno"><br>
				<input type="submit" class="btn btn-primary" name="" value="Registar">
			</form>
		</div>

		<div id="EditorialF" style="display:none" class="row col-md-9">
			<form action="<?php echo base_url('admon/altaEditorial'); ?>" method="post" class="form-group">
				<p>Editorial:</p>
				<input type="text" name="nombreEditorial" class="form-control" placeholder="Editorial" required>
				<p>Dirección editorial:</p>
				<input type="text" name="direccion" id="" class="form-control" placeholder="Dirección" required >
				<input type="submit" class="btn btn-primary" name="" value="Registar">
			</form>
		</div>

		
	</div>

	<div id="Bajas" class="col-md-10" style="display:none">
		<div class="row col-md-9" id="bajasOpciones">
			<a class="btn btn-default btn-lg" href="#" id="LibroB" role="button">Libro</a>
			<a class="btn btn-default btn-lg" href="#" id="AutorB" role="button">Autor</a>
			<a class="btn btn-default btn-lg" href="#" id="EditorialB" role="button">Editorial</a>
		</div>
		
		<div id="LibroFB" class="row col-md-9" style="display:none">

			<form action="<?php echo base_url('admon/bajaLibro') ?>" method="post" class="col-md-6">
				<div class="form-group">
					<select name="libro" id="SelectLibro" required  class="form-control">
						<option value="">Escoje el libro</option>
						<?php 
							foreach ($libros as $key => $libro) {
								# code...
								echo '<option value="'.$libro["id"].'">'.$libro["nombreLibro"].'</option>';
							}
						 ?>
					</select>
				</div>
			<input type="submit" id="btnBaja" class="btn btn-primary" name="" value="Registar">

			</form>

		</div>
		<div id="AutorFB" class="row col-md-9" style="display:none">
			<form action="<?php echo base_url('admon/bajaAutor'); ?>"method="post" class="col-md-6">
				<div class="form-group">
					<select name="autor" id="SelectAutor" required class="form-control">
						<option value="0">Escoje el autor</option>
						<?php 
							foreach ($autores as $key => $autor) {
								# code...
								echo '<option value="'.$autor["idAutor"].'">'.$autor["nombreAutor"].'</option>';
							}
						 ?>
					</select>
				</div>
				 			<input type="submit" id="btnBaja" class="btn btn-primary" name="" value="Registar">
			</form>
		</div>

		<div id="EditorialFB" class="row col-md-9" style="display:none">
			<form action="<?php echo base_url('admon/bajaEditorial'); ?>" method="post" class="col-md-6">
				<div class="form-group">
					<select name="editorial" id="SelectAutor"  class="form-control" required>
						<option value="">Escoje la editorial</option>
						<?php 
							foreach ($editoriales as $key => $editorial) {
								# code...
								echo '<option value="'.$editorial["id"].'">'.$editorial["nombreEditorial"].'</option>';
							}
						 ?>
					</select>
				</div>
				<input type="submit" id="btnBaja" class="btn btn-primary" name="" value="Registar">
			</form>
		</div>
	</div>

	<div id="Modificaciones" class="col-md-10" style="display:none">
		<div class="row col-md-9" id="bajasOpciones">
			<a class="btn btn-default btn-lg" href="#" id="LibroM" role="button">Libro</a>
			<a class="btn btn-default btn-lg" href="#" id="AutorM" role="button">Autor</a>
			<a class="btn btn-default btn-lg" href="#" id="EditorialM" role="button">Editorial</a>
		</div>
		<div id="LibrosM" class="row col-md-9" style="display:none">
			<form action="<?php echo base_url('admon/modLibro'); ?>" method="post" class="col-md-6">
				<div class="form-group">
					<select name="libro" id="SelectLibro"  class="form-control">
						<option value="">Escoje el libro</option>
						<?php 
							foreach ($libros as $key => $libro) {
								# code...
								echo '<option value="'.$libro["nombreLibro"].'">'.$libro["nombreLibro"].'</option>';
							}
						 ?>
					</select>
				</div>
				<div class="form-group">
					<input type="text" class="form-control" name="libroMod" value="" placeholder="Ingresa el nuevo nombre o dejalo en blanco para dejarlo sin cambios">
				</div>
				<div class="form-group">
					<select name="autor" id="SelectAutor"  class="form-control">
						<option value="">Escoje el autor</option>
						<?php 
							foreach ($autores as $key => $autor) {
								# code...
								echo '<option value="'.$autor["idAutor"].'">'.$autor["nombreAutor"].'</option>';
							}
						 ?>
					</select>
				</div>
				<div class="form-group">
					<select name="editorial" id="SelectAutor"  class="form-control" >
						<option value="">Escoje la editorial</option>
						<?php 
							foreach ($editoriales as $key => $editorial) {
								# code...
								echo '<option value="'.$editorial["id"].'">'.$editorial["nombreEditorial"].'</option>';
							}
						 ?>
					</select>
				</div>
				<input type="submit" name="" class="btn btn-primary" value="Registrar">
			</form>

			<div id="ModL">
				
		
			</div>
		</div>
		<div id="AutoresM" class="row col-md-9" style="display:none">
			<form class="col-md-6">
				<div class="form-group">
					<select name="" id="SelectModAutor"  class="form-control">
						<option value="0">Escoje el autor :D</option>
						<?php 
							foreach ($autores as $key => $autor) {
								# code...
								echo '<option value="'.$autor["idAutor"].'">'.$autor["nombreAutor"].'</option>';
							}
						 ?>
					</select>
				</div>
				<div class="form-group"><input type="text" class="form-control" name="autorMod" id="autorMod" value="" placeholder="Cambie el nombre del autor"></div>
				<!--
				<div class="form-group">
					<select name="apAutor" id="SelectAutor"  class="form-control">
						<option value="">Escoje el autor</option>
						<?php 
							foreach ($autores as $key => $autor) {
								# code...
								echo '<option value="'.$autor["apAutor"].'">'.$autor["apAutor"].'</option>';
							}
						 ?>
					</select>
				</div>
				-->
				<div class="form-group">
					<input type="text" name="apMod" id="apMod" class="form-control" value="" placeholder="CAmbie el apellido Paterno">
				</div>
				<!--
				
				<div class="form-group">
					<select name="amAutor" id="SelectAutor"  class="form-control">
						<option value="">Escoje el autor</option>
						<?php 
							foreach ($autores as $key => $autor) {
								# code...
								echo '<option value="'.$autor["amAutor"].'">'.$autor["amAutor"].'</option>';
							}
						 ?>
					</select>
				</div>
				-->	
				<div class="form-group">
					<input type="text" name="amMod" id="amMod" class="form-control" value="" placeholder="Cambie el apellido materno">
				</div>
				<!--
				CREE ESTE CAMPO HIDDEN PARA ESCONDER EL ID
				-->
				<input type="hidden" name="idMod" id="idMod">
				
				<div id="BotonModAutor" class="btn btn-primary">Actualizar</div>
			</form>
			<div id="ModA">
				
			</div>
		</div>
		<div id="EditorialesM" class="row col-md-9" style="display:none">
			<form class="col-md-6">
				<div class="form-group">
					<select name="" id="SelectModEditorial"  class="form-control">
						<option value="">Escoje la editorial</option>
						<?php 
							foreach ($editoriales as $key => $editorial) {
								# code...
								echo '<option value="'.$editorial["id"].'">'.$editorial["nombreEditorial"].'</option>';
							}
						 ?>
					</select>
				</div>
				<div class="form-group">
					<input type="text" name="editMod" id="editMod" value=""  class="form-control" placeholder="CAmbia la editorial">
				</div>
				<div class="form-group">
					<input type="text" name="dirMod" id="dirMod" value="" class="form-control" placeholder="CAmbia la direccion">
				</div>
				<input type="hidden" name="idMod1" id="idMod1">
				
				<div id="BotonModEdit" class="btn btn-primary">Actualizar</div>
			</form>
			 <div id="ModE">
			 	
			 </div>
		</div>
	</div>




</div>