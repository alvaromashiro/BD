<div class="row">
	<h1>Sugerencias</h1>
</div>
<div class="row">
	<p class="row-fluid col-md-10">
		Si tiene alguna sugerencias con respecto al servicio que proporciona Bookies, por facor sea tan amable
		de comentarnos.
	</p>
</div>
<div class="row-fluid">
	<form action="">
		<?php 
		$email = array(
			'name' => 'email', 
			'placeholder' => 'Ingresa tu email',
			'type' => 'email'
			);

		$text = array(
			'name' => 'texto',
			'placeholder' => 'Ingresa tu sugerencia',
			'type' => 'textarea'
			);
		 ?>
		<div class="row col-md-10">
			<?= form_label('Email: ','email'); ?>
			<?=	form_input($email); ?>
		</div>	
		<div class="col-md-10">
			<?= form_label('Sugerencia: ', 'texto'); ?>
			<?=	form_input($text); ?>

		</div>
	</form>
</div>

