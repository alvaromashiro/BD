<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Bookies: La plataforma de intercambio y venta de libros</title>
	
	<?php $url = base_url();  ?>
	<link href="<?= $url?>css/bootstrap.min.css" rel="stylesheet">
	<link href="<?= $url?>css/material.min.css" rel="stylesheet">
	<link href="<?= $url?>css/material-fullpalette.min.css" rel="stylesheet">
	<link href="<?= $url?>css/ripples.min.css" rel="stylesheet">
	<link href="<?= $url?>css/roboto.min.css" rel="stylesheet">
<body>
<nav class="navbar navbar-static-top navbar-material-blue-grey-900">
	  <div class="container-fluid">
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="<?=$url;?>"><img src="<?=base_url();?>img/libros_1.png"  style="height:35px;" alt="Bookies"></a>
	    </div>

	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	      	<li><a href="<?= $url ?>inicio ">Inicio</a></li>
			<li><a href="<?= $url ?>Biblioteca ">Biblioteca</a></li>
	      	<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-exapanded="false">
	      		Servicios
		      	<span class="caret"></span></a>
		      	<ul class="dropdown-menu">
					<li><a href="<?= $url ?>Servicios/Libros">Libros</a></li>
					<!-- <li><a href="<?= $url ?>Servicios/Revistas">Revistas</a></li> -->
					<!-- <li><a href="<?php $url ?>Biblioteca/Promociones">Promociones</a></li>	  Por ahora promociones no-->     			
		      	</ul>	
	      	</li>
	       	<li><a href="<?= $url ?>Sugerencias">Sugerencias</a></li>
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
	
	<div class="container">
		<div class="row-fluid">
			

			<?php
                
                echo isset($contenido)? $contenido: '';
            
			?>
			
		</div>			  
	</div>

	<footer>
		<div class="container">
				<div class="col-md-12 text-center row-fluid" style="margin:20px">
					UPSLP | Base de datos 2015
				</div>
			</div>
	</footer>			

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="<?=$url?>js/bootstrap.min.js"></script>
<script>
	$(document).on('ready',function(){
		// body...
		function plantilla(nombre, autor, editorial) {
    		var html;
	        html+='  <tr>';
	        html+='    <td>'+nombre+'</td>';
	        html+='    <td>'+autor+'</td>';
	        html+='    <td>'+editorial+'</td>';
	        html+='    <td>     ';                  
	       // html+='      <a class="btn btn-default" href="'+base_url+'inicio/verMapa?id='+id+'" role="button">ver Mapa</a>';
	        html+='    </td> ';        
	        html+='  </tr>';
	        return html;
    		
    	}
    	base_url = "<?php echo base_url();?>";

    	$("#InputNombre").on("keypress",function(event) {
    		$("#tabla").css("display","block");
    		if ( event.which == 13 ) { 	//cuando presiones la tecla enter (codigo 13)
		    	event.preventDefault();
				var value = $(this).val(); 
		  		$.ajax({
		  			url : base_url+"servicios/librosAutor",
		  			type : "POST",
		  			data : {nombre:value},
		  		}).done(function(data){
		  			console.log(data);
		  			if(data!=false){
		  				$.each(data, function(index, val) {
		  					$( "#resultados" ).after(plantilla(val["Nombre"],val["autor"],val["editorial"]));
		  				});
		  			}
		  		});	
		  	}
    	});

    	$("#Alta").click(function (){
    		// body...
    		$("#Opciones").show();
    		$("#Bajas").hide();
    		$("#Modificaciones").hide();
    		$("#Altas").show();
    		$("#Libro").click(function() {
    			/* Act on the event */
    			$("#LibroF").show();
    			$("#autorF").hide();
    			$("#EditorialF").hide();

    		});
    		
    		$("#Autor").click(function() {
    			/* Act on the event */
    			$("#autorF").show();
    			$("#LibroF").hide();
    			$("#EditorialF").hide();
    		});

    		$("#Editorial").click(function() {
    			/* Act on the event */
    			$("#autorF").hide();
    			$("#LibroF").hide();
    			$("#EditorialF").show();
    		});
    	});

    	$("#Baja").click(function() {
    		/* Act on the event */
    		$("#Bajas").show();
    		$("#Modificaciones").hide();
    		$("#Altas").hide();
    		$("#Opciones").show();

    		$("#LibroB").click(function() {
    			/* Act on the event */
    			$("#LibroFB").show();
    			$("#AutorFB").hide();
    			$("#EditorialFB").hide();
    		});
    		
    		$("#AutorB").click(function() {
    			/* Act on the event */
    			$("#AutorFB").show();
    			$("#LibroFB").hide();
    			$("#EditorialFB").hide();
    		});

    		$("#EditorialB").click(function() {
    			/* Act on the event */
    			$("#EditorialFB").show();
    			$("#AutorFB").hide();
    			$("#LibroFB").hide();
    		});

    	});

    	$("#Modificacion").click(function() {
    		/* Act on the event */
    		$("#Opciones").show();
    		$("#Bajas").hide();
    		$("#Modificaciones").show();
    		$("#Altas").hide();

    		$('#LibroM').click(function() {
    			$("#LibrosM").show();
    			$("#AutoresM").hide();
    			$("#EditorialesM").hide();
    		});

    		$('#AutorM').click(function() {
    			$("#LibrosM").hide();
    			$("#AutoresM").show();
    			$("#EditorialesM").hide();
    		});

    		$('#EditorialM').click(function() {
    			$("#LibrosM").hide();
    			$("#AutoresM").hide();
    			$("#EditorialesM").show();
    		});

    	});
        $("#SelectModAutor").change(function(){
            var valor = $(this).val();
            $("#idMod").val("");
            $("#autorMod").val("");
            $("#apMod").val("");
            $("#amMod").val("");
            if(valor>0&&valor!=""){
                $.ajax({
                    url:base_url+"admon/modAutor",
                    type:"GET",
                    data:{id:valor}
                }).done(function(data){
                    console.log(data);
                    $("#idMod").val(data.idAutor);
                    $("#autorMod").val(data.nombreAutor);
                    $("#apMod").val(data.apAutor);
                    $("#amMod").val(data.amAutor);
                });
            }
        });

        $("#BotonModAutor").click(function(){
            var id = $("#idMod").val();
            var nombre = $("#autorMod").val();
            var ap = $("#apMod").val();
            var am = $("#amMod").val();
            $.ajax({
                url:base_url+"admon/modAutor",
                type:"POST",
                data:{id:id,nombre:nombre,ap:ap,am:am}
            }).done(function(data){
                console.log(data);
                $("#ModA").text(data.respuesta);
            }).always(function(data){
                console.log(data);
                $("#ModA").html(data.response);
            }).error(function(data){
                $("#ModA").html(data.response);
            });
        });

        $("#SelectModEditorial").change(function(){
            var valor = $(this).val();
            $("#idMod1").val("");
            $("#editMod").val("");
            $("#dir").val("");
            if(valor>0&&valor!=""){
                $.ajax({
                    url:base_url+"admon/modEditorial",
                    type:"GET",
                    data:{id:valor}
                }).done(function(data){
                    console.log(data);
                    $("#idMod1").val(data.id);
                    $("#editMod").val(data.nombreEditorial);
                    $("#dirMod").val(data.dirEditorial);
                });
            }
        });

        $("#BotonModEdit").click(function(){
            var idMod1 = $("#idMod1").val();
            var editMod = $("#editMod").val();
            var dirMod = $("#dirMod").val();
            $.ajax({
                url:base_url+"admon/modEditorial",
                type:"POST",
                data:{idMod1:idMod1,editMod:editMod,dirMod:dirMod}
            }).done(function(data){
                console.log(data);
                $("#ModE").text(data.respuesta);
            }).always(function(data){
                console.log(data);
                $("#ModE").html(data.response);
            }).error(function(data){
                $("#ModE").html(data.response);
            });
        });

        
        
	});

</script>
</body>
</html>
