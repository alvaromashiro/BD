<div class="row-fluid ">
	<h1>Libros</h1>
	<div class="row col-md-9">
		<form action="<?php echo base_url('servicios/busLibros'); ?>" method="post">
			<div class="form-group ">
				
					<p>Nombre:</p>
					<input type="text" required name="libro" class="form-control" placeholder="Ingrese El nombre a buscar">
				
			</div>
			<input type="submit" class="btn btn-primary" value="Buscar">
		</form>
	</div>

	<div class="row col-md-9">
		<form action="<?php echo base_url('servicios/LibrosAutor'); ?>" method="post">
			<div class="form-group ">
				
					<p>Autor:</p>
					<input type="text" name="autor" value="" required class="form-control" placeholder="Ingrese El nombre a buscar">
			</div>
			<input type="submit" name="" class="btn btn-primary" value="Buscar">
		</form>
	</div>

	<div class="row col-md-9">
		<form action="<?php echo base_url('servicios/LibrosEditorial'); ?>" method="post">
			<div class="form-group ">
				
					<p>Editorial:</p>
					<input type="text" name="editorial" value="" required class="form-control" placeholder="Ingrese El nombre a buscar">
				
			</div>
			<input type="submit" name="" class="btn btn-primary" value="Buscar">
		</form>
	</div>
</div>