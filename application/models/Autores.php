<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Autores extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->database();
	}
	
	public function nuevoLibro($libro)
	{
		# code...
		$nuevoLibro = array(
			'nombreLibro' => $libro["libro"],
			'autorLibro' => $libro["autor"],
			'editorialLibro' => $libro["editorial"] 
			);
		$this->db->insert('libros', $nuevoLibro);

	}

	public function nuevoAutor($autor)
	{
		# code...
		$nuevoAutor = array(
			'nombreAutor' => $autor["nombre"], 
			'apAutor' => $autor["apAutor"],
			'amAutor' => $autor["amAutor"] 
			);
		$this->db->insert('autor', $nuevoAutor);
	}

	public function nuevoEditorial($editorial)
	{
		$nuevoEditorial = array(
			'nombreEditorial' => $editorial["nombre"],
			'dirEditorial' => $editorial["dirEditorial"]
			);
		$this->db->insert('editorial', $nuevoEditorial);
	}

	public function getAutores()
	{
		# code...
		$query = $this->db->get('autor');
		$query_result = $query->result();
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	public function getLibros()
	{
		# code...
		$query = $this->db->get('libros');
		$query_result = $query->result();
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}

	public function getEditorial()
	{
		# code...
		$query = $this->db->get('editorial');
		$query_result = $query->result();
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;
		}	
	}

	public function checarLibro($libro)
	{
		# code...
		$this->db->select('nombreLibro');
		$this->db->where('nombreLibro', $libro);
		$query = $this->db->get('libros');
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;
		}	
	}

	public function checarAutor($autor)
	{
		$this->db->select('nombreAutor, apAutor, amAutor');
		$this->db->where('nombreAutor', $autor["nombre"]);
		$this->db->where('apAutor', $autor["apAutor"]);
		$this->db->where('amAutor', $autor["amAutor"]);
		$query = $this->db->get('autor');
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}else{
			return FALSE;
		}	
	}

	public function checarEditorial($editorial)			
	{
		$this->db->select('nombreEditorial,dirEditorial');
		$this->db->where('nombreEditorial', $editorial["nombre"]);
		$this->db->where('dirEditorial', $editorial['dirEditorial']);
		$query = $this->db->get('editorial');
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}else{
			return FALSE;
		}
	}

	public function bajaLibro($libro)
	{
		$this->db->where('id', $libro);
		$this->db->delete('libros');

	}
	
	public function bajaAutor($autor)
	{
		$this->db->where('idAutor', $autor);
		$this->db->delete('autor');
	}

	public function bajaEditorial($editorial)
	{
		$this->db->where('id', $editorial);
		$this->db->delete('editorial');
	}

	public function modLibro($libro)
	{
		# code...
		$libroMod = array(
			'nombreLibro' => $libro["libroMod"],
			'autorLibro' => $libro["autor"],
			'editorialLibro' =>$libro["editorial"]
			);
		$this->db->where('nombreLibro', $libro["libro"]);
		$query = $this->db->update('libros',$libroMod);
		
		
	}

	public function modAutor($autor)
	{
		$this->db->where('idAutor', $autor["idAutor"]);
		unset($autor["idAutor"]);
		$this->db->update('autor', $autor);
	}
	public function getAutor($id)
	{
		$this->db->where('idAutor', $id);
		$query = $this->db->get('autor');
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	public function getEditoriales($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('editorial');
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}

	public function modEditorial($editorial)
	{
		$this->db->where('id', $editorial["id"]);
		unset($editorial["id"]);
		$this->db->update('editorial', $editorial);
	}


}

/* End of file autores.php */
/* Location: ./application/models/autores.php */