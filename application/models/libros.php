<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Libros extends CI_Model {
		
		public function __construct()
		{
			parent::__construct();
			//Do your magic here
			$this->load->database();
		}

		public function getByAutor($autor)
		{
			# code...
			// $query = $this->db->query("SELECT id, autorLibro, editorialLibro, idAutor, nombreAutor, apAutor, amAutor from libros, autor  where nombreAutor like '$autor' or apAutor like '$autor' or amAutor like '$autor' ");
			// $this->db->select('*');
			// $this->db->from('libros');

			$this->db->select('*');
			$this->db->like('nombreAutor',$autor);
			$query = $this->db->get('autor');
			if ($query->num_rows()>0) {
				# code...
				return $query->result_array();
			}else {
				return FALSE;
			}
		}

		public function getByEditorial($editorial)
		{
			# code...
			// $query = $this->db->query("SELECT id, autorLibro, editorialLibro, idAutor, nombreAutor, apAutor, amAutor from libros, autor  where nombreAutor like '$autor' or apAutor like '$autor' or amAutor like '$autor' ");
			// $this->db->select('*');
			// $this->db->from('libros');

			$this->db->select('*');
			$this->db->like('nombreEditorial',$editorial);
			$query = $this->db->get('editorial');
			if ($query->num_rows()>0) {
				# code...
				return $query->result_array();
			}else {
				return FALSE;
			}
		}
		public function getLibros($libro)
		{
			# code...
			$this->db->select('libros.id,libros.nombreLibro,libros.autorLibro,libros.editorialLibro,
							 autor.idAutor,autor.nombreAutor,autor.apAutor,autor.amAutor,
							 editorial.id,editorial.nombreEditorial');
			$this->db->like('nombreLibro',$libro);

			$this->db->join('autor', 'autor.idAutor = libros.autorLibro', 'inner');
			$this->db->join('editorial', 'editorial.id = libros.editorialLibro', 'inner');
			$query = $this->db->get('libros');

			if ($query->num_rows()>0) {
				# code...
				return $query->result_array();
			}else {
				return FALSE;
			}

		}

	// 	public function getDatos_byID($id)
	// {
	// 	$this->db->select('ID,RazonSocial as Nombre,Giro,Domicilio,Colonia');
	// 	$this->db->where("ID",$id);
	// 	$query = $this->db->get('licencias');
	// 	if($query->num_rows()>0){
	// 		return $query->result_array();
	// 	}else{
	// 		return FALSE;
	// 	}
	// }
	// public function getDatos_byNombre($nombre)
	// {
	// 	$this->db->select('ID,RazonSocial as Nombre,Giro,Domicilio,Colonia');
	// 	$this->db->like("RazonSocial",$nombre);
	// 	$query = $this->db->get('licencias',100,0);
	// 	if($query->num_rows()>0){
	// 		return $query->result_array();
	// 	}else{
	// 		return FALSE;
	// 	}
	// }
	// public function getColonias()
	// {
	// 	$this->db->select('Colonia');
	// 	$this->db->distinct();
	// 	$this->db->order_by('Colonia', 'asc');
	// 	$query = $this->db->get('licencias');
	// 	if($query->num_rows()>0){
	// 		return $query->result_array();
	// 	}else{
	// 		return FALSE;
	// 	}
	// }
	// public function getDatos_ByColonia($colonia)
	// {
	// 	$this->db->select('ID,RazonSocial as Nombre,Giro,Domicilio,Colonia');
	// 	$this->db->where("Colonia",$colonia);
	// 	$query = $this->db->get('licencias',100,0);
	// 	if($query->num_rows()>0){
	// 		return $query->result_array();
	// 	}else{
	// 		return FALSE;
	// 	}	
	// }
	
	}
	
	/* End of file Libros.php */
	/* Location: ./application/models/Libros/Libros.php */