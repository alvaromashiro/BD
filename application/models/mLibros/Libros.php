<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Libros extends CI_Model {
		
		public function __construct()
		{
			parent::__construct();
			//Do your magic here
			$this->load->database();
		}

		public function getByAutor($autor)
		{
			# code...
			$this->db->select('id,nombreLibro,autorLibro,editorialLibro');
			$this->db->like('autorLibro',$autor);
			$query = $this->db->get('libros');
			if ($query->num_rows()>0) {
				# code...
				return $query->result_array();
			}else {
				return FALSE;
			}
		}

		public function getDatos_byID($id)
	{
		$this->db->select('ID,RazonSocial as Nombre,Giro,Domicilio,Colonia');
		$this->db->where("ID",$id);
		$query = $this->db->get('licencias');
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	public function getDatos_byNombre($nombre)
	{
		$this->db->select('ID,RazonSocial as Nombre,Giro,Domicilio,Colonia');
		$this->db->like("RazonSocial",$nombre);
		$query = $this->db->get('licencias',100,0);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	public function getColonias()
	{
		$this->db->select('Colonia');
		$this->db->distinct();
		$this->db->order_by('Colonia', 'asc');
		$query = $this->db->get('licencias');
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	public function getDatos_ByColonia($colonia)
	{
		$this->db->select('ID,RazonSocial as Nombre,Giro,Domicilio,Colonia');
		$this->db->where("Colonia",$colonia);
		$query = $this->db->get('licencias',100,0);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;
		}	
	}
	
	}
	
	/* End of file Libros.php */
	/* Location: ./application/models/Libros/Libros.php */