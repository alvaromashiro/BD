<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admon extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->model('autores');
		$this->load->helper('form');
	}

	public function index()
	{	
		$data["editoriales"] =  $this->autores->getEditorial();
		$data["libros"] = $this->autores->getLibros();
		$data["autores"] = $this->autores->getAutores();
		$data["contenido"]=$this->load->view('Admon/Admon',$data, TRUE);		
		$this->load->view('inicio', $data, FALSE);
	}



	public function altaLibro()
	{
		# code...
		$libro["libro"] = $this->input->post('libro');
		$libro["autor"] = $this->input->post('autor');
		$libro["editorial"] = $this->input->post('editorial');
		$result = $this->autores->checarLibro($libro["libro"]);
		if($result==FALSE)
		{
			$this->autores->nuevoLibro($libro);
			redirect('admon','refresh');
		}else{
			redirect('admon','refresh');
		}		
	
		
	}

	public function altaAutor()
	{
		# code...
		$autor["nombre"] = $this->input->post('nombreAutor');
		$autor["apAutor"] = $this->input->post('apAutor');
		$autor["amAutor"] = $this->input->post('amAutor'); 
		$result = $this->autores->checarAutor($autor);
		if($result==FALSE)
		{
			$this->autores->nuevoAutor($autor);
			redirect('admon','refresh');
		}else{
			redirect('admon','refresh');
		}	
	}

	public function altaEditorial()
	{
		# code...
		$editorial["nombre"] = $this->input->post('nombreEditorial');
		$editorial["dirEditorial"] = $this->input->post('direccion');
		$result = $this->autores->checarEditorial($editorial);
		if($result==FALSE)
		{
			$this->autores->nuevoEditorial($editorial);
			redirect('admon','refresh');
		}else{
			redirect('admon','refresh');
		}	
		
	}

	public function bajaLibro()
	{
		$libro = $this->input->post('libro');
		$this->autores->bajaLibro($libro);
		redirect('admon','refresh');
		
	}

	public function bajaAutor()
	{		
		$autor = $this->input->post('autor');
		$this->autores->bajaAutor($autor);
		redirect('admon','refresh');
	}

	public function bajaEditorial()
	{		
		$editorial = $this->input->post('editorial');
		$this->autores->bajaEditorial($editorial);
		redirect('admon','refresh');
	}

	public function modLibro()	
	{
		$libro["libro"] = $this->input->post('libro');
		$libro["libroMod"] = $this->input->post('libroMod');
		$libro["autor"] = $this->input->post('autor');
		$libro["editorial"] = $this->input->post('editorial');
		if($libro["libroMod"]==""){
			$libro["libroMod"] = $libro["libro"];
			$result = $this->autores->modLibro($libro);
			redirect('admon','refresh');
		}elseif (($libro["libroMod"]=="") && ($libro["autor"] =="" )&& ($libro["editorial"]=="")) 
		{
			
			redirect('admon','refresh');
		}else{
			$result = $this->autores->modLibro($libro);
			redirect('admon','refresh');
		}
	}

	public function modAutor()
	{
		if($this->input->is_ajax_request()){
			if($this->input->get()!=null){
				$id = $this->input->get('id', TRUE);
				$data = $this->autores->getAutor($id);
				$array = $data["0"];
				$this->output->set_content_type('application/json')->set_output(json_encode($array));
			}else{
				$datos["idAutor"] = $this->input->post('id', TRUE);
				$datos["nombreAutor"] = $this->input->post('nombre', TRUE);
				$datos["amAutor"] = $this->input->post('am', TRUE);
				$datos["apAutor"] = $this->input->post('ap', TRUE);
				$vacio = FALSE;
				foreach ($datos as $key => $value) {
					if($value==""){
						$vacio = TRUE;
					}
				}
				if($vacio==TRUE){
					$array = array('respuesta' => "Llena correctamente todos los campos", );
					$this->output->set_content_type('application/json')->set_output(json_encode($array));
				}else{
					$this->autores->modAutor($datos);
					$array = array('respuesta' => "Listo", );
					$this->output->set_content_type('application/json')->set_output(json_encode($array));
				
				}
			}	
		}
	}

	public function modEditorial()
	{
		if($this->input->is_ajax_request()){
			if($this->input->get()!=null){
				$id = $this->input->get('id', TRUE);
				$data = $this->autores->getEditoriales($id);
				$array = $data["0"];
				$this->output->set_content_type('application/json')->set_output(json_encode($array));
			}else{
				$datos["id"] = $this->input->post('idMod1', TRUE);
				$datos["nombreEditorial"] = $this->input->post('editMod', TRUE);
				$datos["dirEditorial"] = $this->input->post('dirMod', TRUE);
				$vacio = FALSE;
				foreach ($datos as $key => $value) {
					if($value==""){
						$vacio = TRUE;
					}
				}
				if($vacio==TRUE){
					$array = array('respuesta' => "Llena correctamente todos los campos", );
					$this->output->set_content_type('application/json')->set_output(json_encode($array));
				}else{
					$this->autores->modEditorial($datos);
					$array = array('respuesta' => "Listo", );
					$this->output->set_content_type('application/json')->set_output(json_encode($array));
				
				}
			}	
		}
	}



}

/* End of file Admon.php */
/* Location: ./application/controllers/Admon.php */