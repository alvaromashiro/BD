<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Servicios extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		$this->load->model('libros');
	}

	public function index()
	{
		$data["contenido"] = $this->load->view('contenido',"",TRUE);
		$this->load->view('inicio', $data, FALSE);
	}

	
	
	public function LibrosAutor()
	{
		# code...
		$autor = $this->input->get_post('autor',TRUE);
		//print_r($autor);
		$resultados["autor"] = $this->libros->getByAutor($autor);
		$data["contenido"] = $this->load->view('servicios/autor', $resultados, TRUE);
		$this->load->view('inicio', $data, FALSE);
		
	}

	public function LibrosEditorial()
	{
		# code...
		$editorial = $this->input->get_post('editorial',TRUE);
		$resultados["editorial"] = $this->libros->getByEditorial($editorial);
		// print_r($editorial);
		$data["contenido"] = $this->load->view('servicios/editorial', $resultados, TRUE);
		$this->load->view('inicio', $data, FALSE);
	}

	public function Libros()
	{
		
		$data["contenido"] = $this->load->view('servicios/libros',"", TRUE);
		$this->load->view('inicio', $data, FALSE);
	}
	public function busLibros()
	{
		# code...
		$lib = $this->input->get_post('libro');
		// print_r($lib);
		$libro["libro"] = $this->libros->getLibros($lib);
		// print_r($libro);
		$data["contenido"] = $this->load->view('servicios/busLibros',$libro, TRUE);
		$this->load->view('inicio', $data, FALSE);
	}
	
	

}
